#!/usr/bin/python3

#---------Imports---------

#App modules
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *

#Plotting modules
import matplotlib as mpl
mpl.use("QT5Agg")
import matplotlib.pylab as plt
from matplotlib.backends.backend_qt5agg import FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
from matplotlib.animation import TimedAnimation
import matplotlib.animation as Animation
from matplotlib.lines import Line2D

#Other modules
import sys
import os
import functools
import numpy as np
import random as rd
import scipy
import time
import threading

#Custom imports
import ui_scientific_visualizer
import root_finding
import fun_stuff

#-----------------------------

#----------Theme-------------
styleSheetWhite="""
    QMainWindow{    
            background-color: white;
            color: black;}
    """

styleSheetBlack="""
    QMainWindow{
            background-color:black;}
    QTableWidget{
            background-color:black;
            color: white;}
    QMenuBar{
            background-color:black;
            color: white;}
    QStatusBar{
            background-color:black;
            color: white;}
    QToolBar{
            background-color:black;
            color: white;}
    QWidget{
            background-color:black;
            color: white;}

    QLineEdit{
            background-color:white;
            color: black;}

    """

                

#-----------Classes-----------

class MyMainWindow(QMainWindow):

    def __init__(self):
        #Initial setups and GUI composition
        super().__init__()
        self.ui=ui_scientific_visualizer.Ui_MainWindow()
        self.ui.setupUi(self)
        self.setWindowTitle("Scientific-Visualizer")
        self.setFixedSize(800,578)
        self.status=self.statusBar()
        self.status.setSizeGripEnabled(False)
        self.status.showMessage("Ready to show...",4500)
        self.options_layout=QGridLayout()
        self.ui.widget_Options.setLayout(self.options_layout)

        #Loading previous state
        setting=QSettings()
        self.theme=setting.value("MainWindow/Theme") if  setting.value("MainWindow/Theme") else "Black"

        self.toggleTheme(self.theme)
        self.set_subtype()
        self.set_options()
        self.ui.comboBox.setFocus()

        #----Connections settings-----------
        pixmap=QPixmap(20,20)
        color=QColor(Qt.black)
        pixmap.fill(color)
        self.actionSettings(self.ui.action_Black,pixmap,"",lambda : self.toggleTheme("Black"),"Set black theme")
        
        pixmap=QPixmap(20,20)
        color=QColor(Qt.white)
        pixmap.fill(color)
        self.actionSettings(self.ui.action_White,pixmap,"",lambda : self.toggleTheme("White"),"Set white theme")

        self.actionSettings(self.ui.action_Scientific_visualizer,None,None,self.appHelp,"Show help")
        self.actionSettings(self.ui.actionAbout_the_Developer,None,None,self.appAbout,"Show about the developer")

        self.ui.comboBox.currentIndexChanged.connect(self.set_subtype)
        self.ui.menu_Quit.aboutToShow.connect(lambda: self.closeEvent("needed"))

        self.ui.comboBox_SubPlot.currentIndexChanged[int].connect(lambda: self.set_options())
        self.ui.pushButton_Show.clicked.connect(self.showPlot)



    #----Custom Methods-----------
    def actionSettings(self,action,icon=None,shortcut=None,slot=None,tip=None):
        if icon:
            if isinstance(icon,str):
                action.setIcon(QIcon(f":/{icon}.png"))
            elif isinstance(icon,QPixmap):
                action.setIcon(QIcon(icon))
        if shortcut:
            action.setShortcut(shortcut)
        if slot:
            action.triggered.connect(slot)
        if tip:
            action.setToolTip(tip)
            action.setStatusTip(tip)
                
    def set_subtype(self):
        text=self.ui.comboBox.currentText()
        self.ui.comboBox_SubPlot.clear()
        if text=="Root finding":
            items=["Bisection method","Newton-Rapson method","Regular-Falsi method","Nonlinear system"]
            self.ui.comboBox_SubPlot.addItems(items)
        elif text=="Fun stuff":
            items=["Box particle","Fractals","Double Pendulum"]
            self.ui.comboBox_SubPlot.addItems(items)
        self.ui.comboBox_SubPlot.setFocus()

    def set_options(self):
        while self.options_layout.count():
            item=self.options_layout.takeAt(0)
            widget=item.widget()
            if widget:
                widget.setParent(None)
        text=self.ui.comboBox_SubPlot.currentText()
        if text=="Bisection method":
            self.set_bisection_options()
        elif text=="Box particle":
            self.set_box_particle_options()
        
    def set_bisection_options(self):
        self.equation_line=QLineEdit()
        self.equation_line.setFocus()
        equation_Label=QLabel("f(x)=")
        a=QLabel("a=")
        self.a_spinbox=QSpinBox()
        self.a_spinbox.setRange(-50,50)
        self.a_spinbox.setValue(-5)
        b=QLabel("b=")
        self.b_spinbox=QSpinBox()
        self.b_spinbox.setRange(-50,50)
        self.b_spinbox.setValue(5)
        tol=QLabel("Tol=")
        self.tol_spinbox=QDoubleSpinBox()
        self.tol_spinbox.setRange(0.005,0.5)
        self.tol_spinbox.setValue(0.05)
        self.tol_spinbox.setSingleStep(0.001)
        self.options_layout.addWidget(equation_Label,0,0)
        self.options_layout.addWidget(self.equation_line,0,1)
        self.options_layout.addWidget(a,1,0)
        spacer=QSpacerItem(80,20,QSizePolicy.Expanding,QSizePolicy.Fixed)
        spacer2=QSpacerItem(80,20,QSizePolicy.Expanding,QSizePolicy.Fixed)
        self.options_layout.addWidget(self.a_spinbox,1,1)
        self.options_layout.addItem(spacer,1,2)
        self.options_layout.addItem(spacer,1,3)
        self.options_layout.addItem(spacer,1,4)
        self.options_layout.addWidget(b,2,0)
        self.options_layout.addWidget(self.b_spinbox,2,1)
        self.options_layout.addItem(spacer2,2,2)
        self.options_layout.addWidget(tol,3,0)
        self.options_layout.addWidget(self.tol_spinbox,3,1)

    
    def set_box_particle_options(self):
        spacer=QSpacerItem(80,20,QSizePolicy.Expanding,QSizePolicy.Fixed)

        no_particle=QLabel("No. of Particles:")
        self.particle_box=QSpinBox()
        self.particle_box.setRange(1,100)
        self.particle_box.setValue(5)
        self.particle_box.setFocus()
        self.options_layout.addWidget(no_particle,0,0)
        self.options_layout.addWidget(self.particle_box,0,1)
        self.options_layout.addItem(spacer,0,2)

        size_label=QLabel("Particles' size: " )
        self.particle_size=QComboBox()
        self.particle_size.addItems(["Small","Normal","Large","Very Large"])
        self.particle_size.setCurrentIndex(1)
        self.options_layout.addWidget(size_label,1,0)
        self.options_layout.addWidget(self.particle_size,1,1)

        color_label=QLabel("Particles' color: ")
        self.particle_color=QComboBox()
        self.particle_color.addItems(["Red","Green","Yellow","Blue"])
        self.options_layout.addWidget(color_label,2,0)
        self.options_layout.addWidget(self.particle_color,2,1)

        speed_label=QLabel("Particles' speed: ")
        self.particle_speed=QComboBox()
        self.particle_speed.addItems(["Slow","Normal","Fast","Very Fast"])
        self.particle_speed.setCurrentIndex(1)
        self.options_layout.addWidget(speed_label,3,0)
        self.options_layout.addWidget(self.particle_speed,3,1)

        ghost_label=QLabel("Ghost like: ")
        self.particle_ghost=QComboBox()
        self.particle_ghost.addItems(["Yes","No"])
        self.particle_ghost.setCurrentIndex(1)
        self.particle_ghost.setStatusTip("Enables particles to pass through each other")
        self.particle_ghost.setToolTip("Enables particles to pass through each other")
        self.options_layout.addWidget(ghost_label,4,0)
        self.options_layout.addWidget(self.particle_ghost,4,1)


        
    def showPlot(self):
        text=self.ui.comboBox_SubPlot.currentText()
        if text=="Bisection method":
            equation=self.equation_line.text()
            a=self.a_spinbox.value()
            b=self.b_spinbox.value()
            tol=self.tol_spinbox.value()
            background=self.theme
            root_finding.bisection_method(equation,a,b,tol,background)
        elif text=="Box particle":
            no=self.particle_box.value()
            color=self.particle_color.currentText()
            ghost_like=self.particle_ghost.currentText()
            size=self.particle_size.currentText()
            speed=self.particle_speed.currentText()
            fun_stuff.run_particle_box(no,color,size,speed,ghost_like,theme=self.theme)

            

            


    def toggleTheme(self,theme):
        app=QApplication.instance()
        if app is None:
            raise RuntimeError("App is not running!")
        if theme=="Black":
            app.setStyleSheet(styleSheetBlack)
            self.theme="Black"
        elif theme=="White":
            app.setStyleSheet(styleSheetWhite)
            self.theme="White"
        self.status.showMessage(f"{self.theme} theme set",4500)

    def appHelp(self):
        QMessageBox.about(self,"Scientific-Visualizer: Help","Will be added before the heat death of the universe...")

    def appAbout(self):
        QMessageBox.about(self,"About","This app is developed by Basu Hela.\nEmail: basuhela2@protonmail.com")

    def closeEvent(self,event):
        setting=QSettings()
        setting.setValue("MainWindow/Theme",self.theme)
        self.close()




def main():
    app=QApplication(sys.argv)
    app.setApplicationName("Scientific-Visualizer")
    app.setApplicationVersion("0.2.0")
    window=MyMainWindow()
    window.show()
    sys.exit(app.exec_())

if __name__=='__main__':
    main()


        
