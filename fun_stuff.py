#!/usr/bin/python3

#Imports
import matplotlib as mpl
import matplotlib.pylab as plt
from scipy import integrate
from scipy.spatial.distance import pdist, squareform
from matplotlib import animation
import numpy as np
import time as t

class ParticleBox:
    
    def __init__(self,init_state,box_edges=[-2,2,-2,2],
                particle_size=0.04,gravity=9.8,ghost_like=False):

        self.init_state=np.asarray(init_state,dtype=float)
        self.size=particle_size
        self.particles_masses=self.size*np.ones(self.init_state.shape[0])
        self.state=self.init_state.copy()
        self.time_passed=0
        self.box_edges=box_edges
        self.gravity=gravity
        self.ghost_particles=ghost_like


    def step(self,time):
        """move in every `time` seconds"""
        self.time_passed+=time

        #updating position
        self.state[:,:2]+=self.state[:,2:]*time

        #finding particles under collision
        if not self.ghost_particles:
            self.check_collision()

        #Checking for crossing edges
        self.check_crossing_edges()
        #gravity
        self.state[:,3]-=self.particles_masses*self.gravity*time



    def check_collision(self):
        distance=squareform(pdist(self.state[:,:2]))
        ind1,ind2=np.where(distance<2*self.size)
        unique=(ind1<ind2)
        ind1=ind1[unique]
        ind2=ind2[unique]

        #updating velocities
        for i1,i2 in zip(ind1,ind2):
            m1=self.particles_masses[i1]
            m2=self.particles_masses[i2]

            #location
            l1=self.state[i1,:2]
            l2=self.state[i2,:2]

            #Veclocity
            v1=self.state[i1,2:]
            v2=self.state[i2,2:]

            #Relative location and vecocity
            l_rel=l1-l2
            v_rel=v1-v2
            
            #momemtum vector of the center of mass
            v_cm=(m1*v1+m2*v2) /(m1+m2)

            #Collisions of spheres reflect v_rel over l_rel
            lr_rel=np.dot(l_rel,l_rel)
            vr_rel=np.dot(v_rel,l_rel)
            v_rel = 2 * l_rel * vr_rel / lr_rel - v_rel

            # assign new velocities
            self.state[i1, 2:] = v_cm + v_rel * m2 / (m1 + m2) 
            self.state[i2, 2:] = v_cm - v_rel * m1 / (m1 + m2) 



    def check_crossing_edges(self):
        crossed_x1 = (self.state[:, 0] < self.box_edges[0] + self.size)
        crossed_x2 = (self.state[:, 0] > self.box_edges[1] - self.size)
        crossed_y1 = (self.state[:, 1] < self.box_edges[2] + self.size)
        crossed_y2 = (self.state[:, 1] > self.box_edges[3] - self.size)

        self.state[crossed_x1, 0] = self.box_edges[0] + self.size
        self.state[crossed_x2, 0] = self.box_edges[1] - self.size

        self.state[crossed_y1, 1] = self.box_edges[2] + self.size
        self.state[crossed_y2, 1] = self.box_edges[3] - self.size

        self.state[crossed_x1 | crossed_x2, 2] *= -1
        self.state[crossed_y1 | crossed_y2, 3] *= -1

np.random.seed(int(t.time()))

def run_particle_box(no=5,color="Red",size="Normal",speed="Normal",ghost_like="No",theme="Black"):
    if theme=="Black":
        mpl.style.use('dark_background')
    color=color.lower()[0]
    time=30
    if speed=='Slow':
        time*=2
    elif speed=='Fast':
        time/=2
    elif speed=='Very Fast':
        time/=4
    time=1./time
    particles_size=0.04
    if size=="Small":
        particles_size/=2
    elif size=='Large':
        particles_size*=5
    elif size=='Very Large':
        particles_size*=10

    if ghost_like=='No':
        ghost_like=False
    else:
        ghost_like=True

    #Initial setups
    init_state=-0.5+np.random.random((no,4))
    init_state[:,:2]*=3.1

    box=ParticleBox(init_state,particle_size=particles_size,ghost_like=ghost_like)

    #plotting
    fig=plt.figure()
    fig.subplots_adjust(left=0,right=1,bottom=0,top=1)
    ax=fig.add_subplot(111,aspect='equal',autoscale_on=False,
            xlim=(-3.0,3.0),ylim=(-2.5,2.5))

    ax.set_axis_off()
    particles,=ax.plot([],[],"{}o".format(color),ms=7)

    #Adding rectangle box
    rect=plt.Rectangle(box.box_edges[::2],
                        box.box_edges[1]-box.box_edges[0],
                        box.box_edges[3]-box.box_edges[2],
                        lw=2,fc='none')
    ax.add_patch(rect)

    def init():
        nonlocal box, rect
        particles.set_data([],[])
        rect.set_edgecolor('none')
        return particles,rect

    def animate(i):
        nonlocal box,rect,time,ax,fig
        box.step(time)
        ms= int(fig.dpi*2*box.size*fig.get_figwidth()/np.diff(ax.get_xbound())[0])

        rect.set_edgecolor('b')
        particles.set_data(box.state[:,0],box.state[:,1])
        particles.set_markersize(ms)
        return particles, rect

    ani=animation.FuncAnimation(fig,animate,frames=600,interval=10,
                                blit=True,init_func=init)
    plt.show()








    
    




        




