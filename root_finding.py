#!/usr/bin/python3

import numpy as np
import matplotlib as mpl
import matplotlib.pylab as plt

r=1
def update_r():
    global r
    r+=1

def bisection_method(equation,a,b,tol,background,line_color='red'):
    global r
    r=1
    if background=="Black":
        mpl.style.use("dark_background")
    elif background=="White":
        mpl.style.use("classic")
    
    fig,ax=plt.subplots(1,1,figsize=(12,7))
    fig.suptitle("Bisection method")
    x=np.linspace(a,b,1000)
    try:
        f_x=eval(equation)
    except SyntaxError:
        print("empty string provided!")
        exit(1)
    y_max=max(f_x)+5
    y_min=min(f_x)-5
    
    #plotting
    ax.set_xticks(list(range(a,b+1,1)))
    ax.set_xlabel(r"$x$",fontsize=18)
    ax.set_ylabel(r"$f(x)$",fontsize=18)
    plt.pause(1)
    la_equation=equation.replace("**","^")
    ax.plot(x,f_x,lw=1.5,color=line_color,label=r"$f(x)={}$".format(la_equation))
    ax.set_ylim(y_min,y_max)
    ax.legend()
    plt.pause(1)
    ax.axhline(0,ls=":",color='b')
    ax.set_ylim(y_min,y_max)
    plt.pause(0.5)

    #plotting f(a)
    fa=eval(equation.replace('x','a'))
    ax.plot(a,fa,'bo')
    ax.text(a,fa+0.5,r"$a$",ha='center',color='g')
    plt.pause(0.5)
    ax.vlines(a,0,fa,ls=":",color='g')
    #ax.text(a,-0.5,str(a),ha='center',color='g')
    plt.pause(0.5)

    #plotting f(b)
    fb=eval(equation.replace("x",'b'))
    ax.plot(b,fb,'bo')
    ax.text(b,fb+0.5,r"$b$",ha='center',color='g')
    plt.pause(0.5)
    ax.vlines(b,0,fb,ls=":",color='g')
    #ax.text(b,-0.5,str(b),ha='center',color='g')
    plt.pause(0.5)
    
    while b-a>tol:
        #print(r)
        m=a+(b-a)/2
        f_m=eval(equation.replace('x','m'))
        ax.plot(m,f_m,'bo')
        plt.pause(0.5)
        #ax.text(m,-0.3,str(m),ha='center',color='g')

        if np.sign(fa)==np.sign(f_m):
            a,fa=m,f_m
        else:
            b,fb=m,f_m
        plt.pause(0.5)
        ax.text(m,f_m-0.8,r"$m_{}$".format(r),ha='center',color='g')
        update_r()
        ax.vlines(m,0,f_m,ls=':',color='g')
        plt.pause(0.5)

    ax.plot(m,f_m,'r*',markersize=16)
    plt.pause(0.5)
    ax.annotate("Root approximately at %.3f" % m,
                fontsize=12,xy=(a,f_m),xycoords='data',
                xytext=(-150,+50),textcoords='offset points',
                arrowprops=dict(arrowstyle='->',
                            connectionstyle="arc3,rad=-.5"))
    plt.pause(0.5)

            



    

    


